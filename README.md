# README by N. Sadawi 31/03/2014

This is a simple java OCR that uses the following procedure:
1- Assuming we have a large set of binary character/symbol images 
2- fg is black and bg is white
3- We divide each char image into 9 non-overlapping regions (3x3)
4- For every region, we compute the percentage of fg pixels to the whole area of the region
5- We also compute each region's w/(w+h) (aspect ratio .. kind of!)
6- This gives us 10 real values which we can use as features in our feature vectors
7- For class value, we can use each char's ascii code (3 digits) OR just the char value, such as "N"
8- Example, for 'S' char, a feature vector looks like:
     (0.21,0.23,0.25,0.15,0.22,0.14,0.20,0.21,0.30,0.68,"083")
9- We do this for all our input chars (we should do it only once and save the results to a text file)
10- This text file is now our training database!
11- For a new unknow input character image, we compute the same set of 10 features
12- We loop through all entries in our training database and use Euclidean distance to find the nearest character
13- Using a predefind threshold, we can decide whether the characters matche or the new character is unknown! (Maybe it's not in our traihning db!)
